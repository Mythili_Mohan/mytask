const moment= require('moment')

class utilities{
    
 pageFactory={
     dayInput:''
 }

    
    radioButtonselection(answer){
        cy.get('.sc-d4l-radio>label').each(($el,index,$list)=>{
            const getLable=$el.text()   
            //console.log("test",getLable)
           if(getLable.includes(answer)){
               cy.get(".sc-d4l-radio-h").eq(index).click().should('have.value',index)
           }
    
    
    })
    
     }
     checkBoxButtonSelection(answer){
        cy.get('.sc-d4l-checkbox>label').each(($el,index,$list)=>{
            const getlable=$el.text()
            console.log("gfhhgfg",getlable)
            if(getlable.includes(answer)){
                cy.get(".sc-d4l-checkbox-h").eq(index).click()
                
            }

        })
     }
     getDate(){
         var getCurrentDate=new Date()
         var getMonth= moment().subtract(14,'day').get('month')+1.
         var getDay= moment().subtract(14,'day').get('date')
         var getYear= moment().subtract(14,'day').get('year')
         var getCurrentMonth =moment().get('month')+1
         var getCurrentDay= moment().get('date')
         var getCurrentYear= moment().get('year')

         
          //console.log(getMonth,getDay,getYear)
          return {getCurrentDate,getDay,getMonth,getYear,getCurrentDay,getCurrentMonth,getCurrentYear}
          
          
                }

questionAndAnswer(question,answer){
    switch(question){
        case 'How old are you':
            this.radioButtonselection(answer);
            console.log(this.getDate().getDay,this.getDate().getMonth,this.getDate().getYear)

        break;
        case 'Are you 65 years old or older':
            this.radioButtonselection(answer);
        break;
        case 'What is your current living situation':
            this.radioButtonselection(answer)
        break;
        case 'At least once a week, do you privately care for people with age-related conditions, chronic illnesses, or frailty?':
            this.radioButtonselection(answer)
        break;
        case 'Do you work in one of the following areas':
            this.radioButtonselection(answer)
        break;
        case 'Do you smoke?':
            this.radioButtonselection(answer)
        break;
        case 'Are you pregnant?':
            this.radioButtonselection(answer)
        break;
        case 'Have you had close contact with a confirmed case?':
            this.radioButtonselection(answer)
        break;
        case 'What day was the last contact?':
            cy.get("#day__0 > .input > .input__element").type(this.getDate().getDay)
            cy.get("[id='month__1']>div").type(this.getDate().getMonth)
            cy.get('[id="year__2"]>div').type(this.getDate().getYear)
        break;
        case 'In the past 24 hours, have you had a fever (over 38°C)?':
            this.radioButtonselection(answer)
        break;
        case 'In the past 4 days, have you had a fever (over 38°C)?':
            this.radioButtonselection(answer)
        break;
        case 'In the past 24 hours, which of the following symptoms have you had? (multiple selection possible)':
            this.checkBoxButtonSelection(answer)
        break;
        case 'In the past 24 hours, which of the following symptoms have you had? (multiple selection possible)':
            this.checkBoxButtonSelection(answer)
         break;
            case 'In the past 24 hours, did you feel that you were more quickly out of breath than usual':
            this.radioButtonselection(answer)
        break;
        case 'With regard to all questions about symptoms: since when have you had the symptoms you specified?':
            cy.get("#day__0 > .input > .input__element").type(this.getDate().getCurrentDay)
            cy.get("[id='month__1']>div").type(this.getDate().getCurrentMonth)
            cy.get('[id="year__2"]>div').type(this.getDate().getYear)
        break;
        case 'Have you been diagnosed with chronic lung disease by a doctor?':
            this.radioButtonselection(answer)
        break;
        
        case 'Have you been diagnosed with diabetes by a doctor?':
            this.radioButtonselection(answer)
        break;
        case 'Have you been diagnosed with heart disease by a doctor?':
            this.radioButtonselection(answer)
        break;
        case 'Have you been diagnosed with obesity by a doctor?':
            this.radioButtonselection(answer)
        break;
        case 'Are you currently taking steroids?':
            this.radioButtonselection(answer)
        break;

        case 'Are you currently taking immunosuppressants?':
            this.radioButtonselection(answer)
        break;
        case 'Have you been vaccinated against flu between October 2019 and today?':
            this.radioButtonselection(answer)
        break;
        
        case  'Consent to the transmission of your postal code and recommended action':
            this.radioButtonselection(answer)
        break;
        
        

    }
}
 
}
export default utilities ;