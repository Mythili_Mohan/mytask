class commonFunctions{

    clickOnStartQuestionnaire () {
     return cy.get('#d4l-button-register').contains('Start questionnaire').click()
        
    }
    clickContinueToQuestion(){
        return cy.get('[href="/questionnaire"]').should('have.text','Continue to questions').click()
    }
    clickNext(){
        return cy.get('[type="submit"]').click()
    }
    summaryPage(){
        cy.get('[class="summary__content"]>h2').should('have.text','Your summary')
    }
    clickLearnMoreLink(){
        cy.get('[href="https://app.data4life.care/corona/?source=CovApp"]').invoke('removeAttr','target').click()
        
    }
}
export default commonFunctions