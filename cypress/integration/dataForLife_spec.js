/// <reference types="Cypress" />
import CommonFuns from '../support/pageObjects/commonFunctions.js'
import Utils from '../support/utilities.js'


describe("End to End",function() {
before(function () {
    cy.fixture('properties').then(function(data) {
        this.data=data;
        
    })
    
})

const commonFunctions=new CommonFuns()
const questionnaireScreen=new Utils()

it("Questionnaire-Out of Breath Symptom", function() {

    cy.visit(this.data.url)

    commonFunctions.clickOnStartQuestionnaire()
    commonFunctions.clickContinueToQuestion()

    questionnaireScreen.questionAndAnswer(this.data.Qus1,this.data.Ans1)
    commonFunctions.clickNext()
    
    questionnaireScreen.questionAndAnswer(this.data.Qus2,this.data.AnsNo)
    commonFunctions.clickNext()
    
    questionnaireScreen.questionAndAnswer(this.data.Qus3,this.data.Ans3)
    commonFunctions.clickNext()
   
    questionnaireScreen.questionAndAnswer(this.data.Qus4,this.data.AnsNo)
    commonFunctions.clickNext()
   
    questionnaireScreen.questionAndAnswer(this.data.Qus5,this.data.Ans5)
    commonFunctions.clickNext()

    questionnaireScreen.questionAndAnswer(this.data.Qus6,this.data.AnsNo)
    commonFunctions.clickNext()

    questionnaireScreen.questionAndAnswer(this.data.Qus7,this.data.AnsNo)
    commonFunctions.clickNext()
    //Requirement 1 -(a)
    questionnaireScreen.questionAndAnswer(this.data.Qus8,this.data.AnsYes)
    commonFunctions.clickNext()
    //Requirement 1 -(b)    
    questionnaireScreen.questionAndAnswer(this.data.Qus9)
    commonFunctions.clickNext()

    questionnaireScreen.questionAndAnswer(this.data.Qus10,this.data.AnsNo)
    commonFunctions.clickNext()

    questionnaireScreen.questionAndAnswer(this.data.Qus11,this.data.AnsNo)
    commonFunctions.clickNext()

    questionnaireScreen.questionAndAnswer(this.data.Qus12,this.data.Ans12)
    commonFunctions.clickNext()

    questionnaireScreen.questionAndAnswer(this.data.Qus13,this.data.Ans13)
    commonFunctions.clickNext()
    //Requirement 1 -(c)
    questionnaireScreen.questionAndAnswer(this.data.Qus14,this.data.AnsYes)
    commonFunctions.clickNext()
     //Requirement 1 -(d)
    questionnaireScreen.questionAndAnswer(this.data.Qus15)
    commonFunctions.clickNext()

    questionnaireScreen.questionAndAnswer(this.data.Qus16,this.data.AnsIdknow)
    commonFunctions.clickNext()

    questionnaireScreen.questionAndAnswer(this.data.Qus17,this.data.AnsNo)
    commonFunctions.clickNext()

    questionnaireScreen.questionAndAnswer(this.data.Qus18,this.data.AnsIdknow)
    commonFunctions.clickNext()

    questionnaireScreen.questionAndAnswer(this.data.Qus19,this.data.AnsIdknow)
    commonFunctions.clickNext()

    questionnaireScreen.questionAndAnswer(this.data.Qus20,this.data.AnsIdknow)
    commonFunctions.clickNext()

    questionnaireScreen.questionAndAnswer(this.data.Qus21,this.data.AnsIdknow)
    commonFunctions.clickNext()

    questionnaireScreen.questionAndAnswer(this.data.Qus22,this.data.AnsNo)
    commonFunctions.clickNext()

    questionnaireScreen.questionAndAnswer(this.data.Qus23,this.data.AnsNo)
    commonFunctions.clickNext()
    commonFunctions.summaryPage()
    commonFunctions.clickLearnMoreLink()
    cy.go('back')


    





    
    
})
 
})



